#include "transversal.h"

//privados
void CargaEstados(tPtroGral*, tCad);
void CargaSigma(tPtroGral*, tCad);
void cargarDelta(tPtroGral*, tCad);
void cargarEstadosNuevos(tPtroGral*,tPtroGral,int,tPtroGral);/*para cargar el conjunto de los nuevos estados dependiendo
los estados actuales el caracter y la funcion de transicion*/
int buscaEstado(tPtroGral,tPtro_String);/*retorna 1 si lo encontro 0 si no lo encontro*/






tPtroEntero Nuevo_nodoE(int E){
	tPtroEntero nuevo;
	nuevo=(tNodoEntero*)malloc(sizeof( tNodoEntero));
	nuevo->tipoNodo=1;
	nuevo->dato=E;
	return nuevo;
	
}

tPtro_String Nuevo_nodo_S(tCad cadena){
	tPtro_String nuevo;
	nuevo=(tNodo_String*)malloc(sizeof(tNodo_String));
	nuevo->tipoNodo=2;
	strcpy(nuevo->dato,cadena);
	//nuevo->dato=cadena;
	
	return nuevo;
}

tPtroConjunto NuevoNodoConj(){
	tPtroConjunto nuevo;
	nuevo=(tNodoConjunto*)malloc(sizeof(tNodoConjunto));
	nuevo->tipoNodo=3;
	nuevo->dato=NULL;
	nuevo->sig=NULL;
	return nuevo;
}

tPtroLista NuevoNodoList(){
	tPtroLista nuevo;
	nuevo=(tNodoLista*)malloc(sizeof(tNodoLista));
	nuevo->tipoNodo=4;
	nuevo->dato=NULL;
	nuevo->sig=NULL;
	return nuevo;
}

tPtroGral NuevoNodoGral(){
	tPtroGral nuevo;
	nuevo=(tNodo*)malloc(sizeof(tNodo));
	nuevo->dato=NULL;
	nuevo->sig=NULL;
	return nuevo;
	
}

void CargarAF(tPtroGral* list ,tCad estados,tCad Sigma,tCad transiciones,tCad estadoInicial,tCad estadosDeAceptacion){
	//*list=(tPtroGral)NuevoNodoList();
	tPtroGral aux;
	CargaEstados(list,estados);
	aux=(*list);
	CargaSigma(&((*list)->sig),Sigma);/*&((*list)->sig para enlazar el siguiente al nuevo list que se creara dendro del modulo*/
	*list=(*list)->sig;
	cargarDelta(&((*list)->sig),transiciones);
	*list=(*list)->sig;
	(*list)->sig=(tPtroGral)NuevoNodoList();
	*list=(*list)->sig;
	(*list)->dato=(tPtroGral)Nuevo_nodo_S(estadoInicial);
	CargaEstados(&((*list)->sig),estadosDeAceptacion);
	
	
	
	
	*list=aux;
}

int cadenaAceptada(tPtroGral list,tCad cadena){
	tPtroGral aux;
	tPtroGral estadosActuales;// conjunto  de estadosActuales
	tPtroGral estadosNuevos;
	estadosActuales=(tPtroGral)NuevoNodoConj();
	int caracter;//en este caso tengo definido el alfabeto para int
	list=list->sig;
	list=list->sig;
	aux=list;
	list=list->sig;
	estadosActuales->dato=(list->dato);//estado inicial
	list=aux;
	list=list->dato;//set
	if(!cadenaVacia(cadena)){
		estadosNuevos=(tPtroGral)NuevoNodoConj();
		aux=list;
		caracter=(int)(retornaCaracter(cadena)-48);//paso de caracter a entero
		cargarEstadosNuevos(&estadosNuevos,estadosActuales,caracter,list);
		estadosActuales=estadosNuevos;
		while(!cadenaVacia(cadena)){
			caracter=(int)(retornaCaracter(cadena)-48);//paso de caracter a entero
			cargarEstadosNuevos(&estadosNuevos,estadosActuales,caracter,list);
			estadosActuales=estadosNuevos;
		}
		//hacer la interseccion  de los conjuntos de aceptacion y los estados actuales
		
		
	}
	
}
void cargarEstadosNuevos(tPtroGral* estadosNuevos,tPtroGral estadosActuales,int caracter,tPtroGral list){
	tPtroGral aux,ant,aux2;
	aux2=(*estadosNuevos);
	while(estadosActuales!=NULL){
		while(list!=NULL){
			aux=list;
			list=list->dato;
			if(comparaEstados((tPtro_String)(list->dato),(tPtro_String)(estadosActuales->dato))==0){
				//ant=list;
				list=list->sig;//
				//list=list->dato;
				if(((tPtroEntero)(list->dato))->dato==caracter){
					//list=ant;
					list=list->sig;
					/*antes ver si no esta repetido*/
					if(buscaEstado(*estadosNuevos,(tPtro_String)(list->dato))==0 ){
						(*estadosNuevos)->dato=list->dato;
						(*estadosNuevos)->sig=(tPtroGral)NuevoNodoConj();
						ant=(*estadosNuevos);
						*estadosNuevos=(*estadosNuevos)->sig;
					}
				}
			}
			list=aux;
			list=list->sig;
		}
		estadosActuales=estadosActuales->sig;
	}
	*estadosNuevos=ant;
	(*estadosNuevos)->sig=NULL;
	*estadosNuevos=aux2;
	
}

int buscaEstado(tPtroGral list,tPtro_String estado){/*retorna 1 si lo encontro 0 si no lo encontro*/
	while(list!=NULL && comparaEstados( ((tPtro_String)list->dato),estado)!=0){
		list=list->sig;
	}
	if(list!=NULL){
		return 1;
	}
	else
	   return 0;
}

int comparaEstados(tPtro_String estado1,tPtro_String estado2){//retorna 0 en caso que sean iguales
	if(comparaCadenas(estado1->dato,estado2->dato)){
		return 0;
	}
	else
		return -1;
	
}

void cargarDelta(tPtroGral* list, tCad elementos){
	tPtroGral aux,ant;
	tCad token1,token2;
	
	*list=(tPtroGral)NuevoNodoList();
	(*list)->dato=(tPtroGral)NuevoNodoConj();
	aux=(*list);
	
	*list=(*list)->dato; //set
	separador(token1,elementos,'-');
	while(!cadenaVacia(token1)){
		ant=(*list);//set
		(*list)->dato=(tPtroGral)NuevoNodoList();
		*list=(*list)->dato;
		PreparaTrancicion(token1);
		separador(token2,token1,',');
		(*list)->dato=(tPtroGral)Nuevo_nodo_S(token2);
		(*list)->sig=(tPtroGral)NuevoNodoList();
		*list=(*list)->sig;
		separador(token2,token1,',');
		(*list)->dato=(tPtroGral)Nuevo_nodoE((int)atoi(token2));
		(*list)->sig=(tPtroGral)NuevoNodoList();
		*list=(*list)->sig;
		separador(token2,token1,',');
		if(strcmp(token2,"{}")!=0){
			(*list)->dato=(tPtroGral)Nuevo_nodo_S(token2);
		}else
		   (*list)->dato=NULL;
		
		*list=ant;
		(*list)->sig=(tPtroGral)NuevoNodoConj();
		*list=(*list)->sig;
		separador(token1,elementos,'-');
	}
	*list=ant;
	(*list)->sig=NULL;
	(*list)=aux;
}

void CargaEstados(tPtroGral* list, tCad elementos){
	tPtroGral aux,ant;
	tCad token;
	//token=strtok(elementos,",");
	separador(token,elementos,',');
	*list=(tPtroGral)NuevoNodoList();
	aux=(*list);
	(*list)->dato=(tPtroGral)NuevoNodoConj();/*(tPtroGral)*/
	(*list)=(*list)->dato;
	
	while(!cadenaVacia(token)){
		ant=(*list);
		(*list)->dato=(tPtroGral)Nuevo_nodo_S(token);/*(tPtro_String)*/
		(*list)->sig=(tPtroGral)NuevoNodoConj();
		(*list)=(*list)->sig;
		separador(token,elementos,',');
	}
	ant->sig=NULL;/*para evitar tener un set de sobra*/
	(*list)=aux;
}

void CargaSigma(tPtroGral* list, tCad elementos){
	tCad token;
	tPtroGral aux,ant;
	separador(token,elementos,',');
	*list=(tPtroGral)NuevoNodoList();
	aux=(*list);
	(*list)->dato=(tPtroGral)NuevoNodoConj();
	(*list)=(*list)->dato;
	
	while(!cadenaVacia(token)){
		
		ant=(*list);
		(*list)->dato=(tPtroGral)Nuevo_nodoE((int)atoi(token));
		(*list)->sig=(tPtroGral)NuevoNodoConj();
		*list=(*list)->sig;
		
		separador(token,elementos,',');
	}
	ant->sig=NULL;//para no tener un set de mas
	*list=aux;
	
}


/*
void obtenerEstado(tCad cadena, tPtroGral list,int nro){
	list=list->dato;
	//list=(tPtro_String)list->dato;
	//cadena=((tPtro_String)(list->dato))->dato;
	//cadena=list->dato;
	for(i=1;i<4,i++){
		list=list->sig;
	}
	strcpy(cadena,((tPtro_String)(list->dato))->dato);
	
	
}
*/

void MostrarEstados(tPtroGral list){
	list=list->dato;
	while(list!=NULL){
		printf("%s \n" ,((tPtro_String)(list->dato))->dato);
		list=list->sig;
	}
}

void mostrarSigma(tPtroGral list){
	list=list->sig;
	list=list->dato;
	while(list!=NULL){
		printf("%i \n" ,((tPtroEntero)(list->dato))->dato);
		list=list->sig;
	}
	
}

void mostrarFuncionDeTransicion(tPtroGral list){
	tPtroGral aux;
	list=list->sig;
	list=list->sig;
	list=list->dato;
	
	while(list!=NULL){
		aux=list;
		list=list->dato;
		printf("%s \n" ,((tPtro_String)(list->dato))->dato);
		list=list->sig;
		printf("%i\n" ,((tPtroEntero)(list->dato))->dato);
		list=list->sig;
		if(list->dato!=NULL){
			printf("%s \n" ,((tPtro_String)(list->dato))->dato);
		}else
		   printf("vacio\n");
		list=aux;
		list=list->sig;
		printf("\n");
	}
	
}

void mostrarAF(tPtroGral list){
	printf("\nestados\n");
	MostrarEstados(list);
	printf("\nsigma\n");
	mostrarSigma(list);
	printf("\nTransiciones: \n");
	mostrarFuncionDeTransicion(list);
	list=list->sig;
	list=list->sig;
	list=list->sig;
	printf("\nEstadoInicial\n");
	printf("%s \n",((tPtro_String)(list->dato))->dato);
	list=list->sig;
	printf("\nestados de aceptacion\n");
	MostrarEstados(list);
	
}
