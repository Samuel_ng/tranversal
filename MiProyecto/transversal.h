#ifndef TRANSVERSAL_H
#define TRANSVERSAL_H

#include "general.h"
//tiponodo;  1.entero 2.cadena 3.conjuto 4.list

typedef struct Nodo{
	int tipoNodo;
	struct Nodo *dato;
	struct Nodo* sig;
}tNodo;
typedef tNodo* tPtroGral;


typedef struct NodoLista{
	int tipoNodo;
	struct NodoLista* dato;
	struct NodoLista* sig;
}tNodoLista;
typedef tNodoLista* tPtroLista;


typedef struct NodoConjunto{
	int tipoNodo;
	struct NodoConjunto* dato;
	struct NodoConjunto* sig;
}tNodoConjunto;
typedef tNodoConjunto* tPtroConjunto;


typedef struct NodoEntero{
	int tipoNodo;
	int dato;
}tNodoEntero;
typedef tNodoEntero* tPtroEntero;


typedef struct Nodo_String{
	int tipoNodo;
	tCad dato;
}tNodo_String;
typedef tNodo_String* tPtro_String;



//talvez deberian ir ocultos ahre
tPtroEntero Nuevo_nodoE(int);
tPtro_String Nuevo_nodo_S(char*);
tPtroConjunto NuevoNodoConj();
tPtroLista NuevoNodoList();
tPtroGral NuevoNodoGral();

void CargarAF(tPtroGral*,tCad,tCad,tCad,tCad,tCad);
void mostrarAF(tPtroGral);
int comparaEstados(tPtro_String,tPtro_String);


#endif
