#ifndef GENERAL_H
#define GENERAL_H



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>


#define TAM 200

typedef char tCad[TAM];


void LeeCad(tCad, int);
void PreparaTrancicion(tCad);/* transforma la cadena de la forma (a,b,c) en a,b,c para su poterior tratado */
void separador(tCad,tCad,char);
bool cadenaVacia(tCad);
bool comparaCadenas(tCad,tCad);
char retornaCaracter(tCad);/*retorna el primer caracter y elimina el mismo de la cadena*/

#endif
