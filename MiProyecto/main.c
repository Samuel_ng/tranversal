#include <stdio.h>
#include "transversal.h"

int main(void){
	//tCad cadena;
	tPtroGral AF;
	AF=NULL;
	tCad estados,sigma,transiciones,estadoInicial,estadosDeAceptacion;
	/*printf("escribir los estados separados por comas\nEjemplo: q1,q2,q3,q4\nr1,r2,r3,r4,r5\n");
	LeeCad(estados,TAM);
	printf("\nescribir los numeros separados por comas\nEjemplo: 1,2,3,4,5\n");
	LeeCad(sigma,TAM);
	printf("\nescribir las transiciones separadas de la siguiente forma por \nejemplo: (q1,0,q1)-(q1,1,q2) \n se usara {} para vacio\n");
	LeeCad(transiciones,TAM);
	printf("\nescribir estado inicial:\n");
	LeeCad(estadoInicial,TAM);
	printf("\nescribir estados de aceptacion de la siguiente forma\npor ejemplo q1,q2,q3,q4\nr1,r2,r3,r4,r5\n");
	LeeCad(estadosDeAceptacion,TAM);
	*/
	
	strcpy(estados,"q0,q1,q2,q3,q4,q5");
	strcpy(sigma,"0,1");
	strcpy(transiciones,"(q0,0,q0)-(q0,1,q1)-(q1,0,q2)-(q1,1,q3)-(q2,0,q4)-(q2,1,q5)-(q3,0,q0)-(q3,1,q1)-(q4,0,q2)-(q4,1,q3)-(q5,0,q4)-(q5,1,q5)");
	strcpy(estadoInicial,"q0");
	strcpy(estadosDeAceptacion,"q1");
	CargarAF(&AF,estados,sigma,transiciones,estadoInicial,estadosDeAceptacion);
	mostrarAF(AF);
	/*obtenerEstado(cadena,AF);
	printf(cadena);*/
	
	return 0;
}
